import { Livro } from './livro';

export const LIVROS: Livro[] = [
    { id: 0, nome:'Dungeons and Dragons', versao:'3.5.6', qtd_criaturas:685, qtd_itens: 300, qtd_regras: 340},
    { id: 0, nome:'Dungeons and Dragons', versao:'4.3', qtd_criaturas:727, qtd_itens: 462, qtd_regras: 220},
    { id: 0, nome:'O Um Anel', versao:'1', qtd_criaturas:279, qtd_itens: 67, qtd_regras: 250},
    { id: 0, nome:'Xablando', versao:'2', qtd_criaturas:30, qtd_itens: 23, qtd_regras: 10},
    { id: 0, nome:'O Mundo', versao:'Alfa', qtd_criaturas:240, qtd_itens: 389, qtd_regras: 270},
    { id: 0, nome:'AL4M World', versao:'0.1.1', qtd_criaturas:35, qtd_itens: 40, qtd_regras: 150},
    { id: 0, nome:'Ragnarok off-Line', versao:'Third', qtd_criaturas:1670, qtd_itens: 864, qtd_regras: 573},
    { id: 0, nome:'Pink Panther', versao:'2.1', qtd_criaturas:8, qtd_itens: 40, qtd_regras: 80}
    //{ id: 0, nome:'', versao:'', qtd_criaturas:0, qtd_itens: 0, qtd_regras: 0}
];