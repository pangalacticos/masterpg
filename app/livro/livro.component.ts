import { Component, OnInit }    from '@angular/core';

import { Livro }        from './livro';
import { LivroService } from './livro.service';

@Component({
    selector: 'livro',
    templateUrl: 'app/livro/livro.html',

    providers: [LivroService]
})
export class LivroComponent {
    constructor(private livroService:LivroService){}

    livro: Livro;
    livros: Livro[];

    getLivros(): void {
        this.livroService.getLivros().then(livros => this.livros = livros);
    }

    ngOnInit(): void{
        this.getLivros();
    }
}