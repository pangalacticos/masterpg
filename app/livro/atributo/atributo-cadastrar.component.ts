import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';

import { AtributoService }          from './atributo.service'
import { Atributo }                 from './atributo';

@Component({
    selector: 'atributo-cadastrar',
    templateUrl: 'app/livro/atributo/atributo.cadastrar.html',

    providers: []
})
export class AtributoCadastrarComponent implements OnInit {
    constructor(
        private atributoService: AtributoService,
        private route: ActivatedRoute ){}

    atributo: Atributo;

    ngOnInit(): void {
        this.route.params.forEach((params: Params) => {
            let id = +params['id'];
            this.atributoService.getAtributo(id)
            .then(atributo => this.atributo = atributo);
        })
    };
    voltar(): void {
        window.history.back();
    };
    salvar(): void{
        this.atributoService.atualizar(this.atributo)
            .then(this.voltar);
    };
    /*adicionar(atributo): void{
      if (!atributo.nome.trim()) { return; }
      atributo.id=atributo.id+1;
      this.atributoService.criar(atributo)
        .then(this.voltar);
    };
    remover(atributo): void {
        this.atributoService
            .apagar(atributo.id)
            .then(this.voltar);
    }*/

}