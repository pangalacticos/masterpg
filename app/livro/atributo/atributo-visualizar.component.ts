import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

import { Atributo }                 from './atributo';
import { AtributoDetailComponent }  from './atributo-detail.component';
import { AtributoService }          from './atributo.service';

@Component({
    selector: 'atributo-visualizar',
    templateUrl: 'app/livro/atributo/atributo.visualizar.html',

    providers: []
})
export class AtributoVisualizarComponent implements OnInit {
    atributos: Atributo[] = [];
    atrib: Atributo;
    mode = 'Observable';

    constructor(
        private router: Router,
        private atributoService: AtributoService
    ) { }

    ngOnInit(): void {
        this.getAtributos();
    }

    atributoSelecionado: Atributo;

    onSelect(atributo: Atributo): void {
        this.atributoSelecionado = atributo;

    }

    vaEditar(atributo: Atributo): void {
        let link = ['/livro/detalhes/atributos', atributo.id];
        this.router.navigate(link);
    }

    getAtributos(): void {
        this.atributoService.getAtributos()
            .subscribe(
                atributos => this.atributos = atributos
            );
    }//*/
/*    getAtributos(): void {
        this.atributoService.getAtributos()
            .then(
                atributos => this.atributos = atributos
            );
    }//*/

}