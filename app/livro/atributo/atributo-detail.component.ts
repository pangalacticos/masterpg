import { Component, Input } from '@angular/core';

import { Atributo }             from './atributo';

@Component({
  selector: 'atributo-detail',
  template: `
  <div class="row" *ngIf="atributo">
    <h2>Detalhes do {{atributo.nome}} !</h2>
    <div><label>id: </label>{{atributo.id}}</div>
    <div><label>nome: </label><input [(ngModel)]="atributo.nome" placeholder="nome"/></div>
    <div><label>nome: </label><input [(ngModel)]="atributo.sigla" placeholder="sigla"/></div>
    <div><label>nome: </label><input [(ngModel)]="atributo.dinamico" placeholder="tipo"/></div>
</div>
`
})
export class AtributoDetailComponent {
    @Input()
    atributo: Atributo;
}

