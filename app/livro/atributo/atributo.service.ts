import { Injectable }   from '@angular/core';
import { Headers, Http, Response, Jsonp } from '@angular/http';

import 'app/rxjs-operators';
import { Observable }     from 'rxjs/Observable';

import { Atributo }     from './atributo';

@Injectable()
export class AtributoService {
    private atributosUrl = 'http://192.168.1.102:8080/MasteRevreS/atributos/';
    private jsonp: Jsonp;
    private headers = new Headers({ 'Content-Type': 'application/json' });

    constructor(private http: Http) { }

    private handleError(error: any): Promise<any> {
        console.error('Ocorreu um erro', error);
        return Promise.reject(error.mensage || error);
    }

/*    getAtributos(): Promise<Atributo[]> {
        return this.jsonp.request(this.atributosUrl, { method: 'Get' })
            .map(this.extractData)
            .toPromise()
            .then(response => response.json().data as Atributo[])
            .catch(this.handleError);
    }
    private extractData(res: Response) {
        let body = res.json();
        console.log(body.data);
        return body.data || {};
    }//*/

/*        getAtributos(): Promise<Atributo[]> {
            return this.http.get(this.atributosUrl)
                .toPromise()
                .then(response => response.json().data as Atributo[])
                .catch(this.handleError);
        }//*/

/*        getAtributos(): Promise<Atributo[]> {
            return this.http.get(this.atributosUrl)
                .toPromise()
                .then(response => response.json().data as Atributo[])
                .catch(this.handleError);
        }
        private extractData(res: Response) {
            let body = res.json();
            return body.data || {};
        }//*/
        
        getAtributos(): Observable<Atributo[]> {
            return this.http.get(this.atributosUrl)
                .map((res:Response) => res.json() as Atributo[])
                .catch(this.handleError);
        }

    getAtributo(id: number): Promise<Atributo> {
        return this.getAtributos().toPromise()
            .then(atributos => atributos.find(atributo => atributo.id === id));
    }

    atualizar(atributo: Atributo): Promise<Atributo> {
        const url = `${this.atributosUrl}/${atributo.id}`;
        return this.http
            .put(url, JSON.stringify(atributo), { headers: this.headers })
            .toPromise()
            .then(() => atributo)
            .catch(this.handleError);
    }

    criar(atributo: Atributo): Promise<Atributo> {
        return this.http
            .post(this.atributosUrl, JSON.stringify({ atributo: Atributo }), { headers: this.headers })
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError);
    }
    apagar(id: number): Promise<void> {
        let url = `${this.atributosUrl}/${id}`;
        return this.http.delete(url, { headers: this.headers })
            .toPromise()
            .then(() => null)
            .catch(this.handleError);
    }
}