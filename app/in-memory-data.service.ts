import { InMemoryDbService } from 'angular2-in-memory-web-api';
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    let atributos = [
    {   id:1,   nome:'Dextreza',    sigla:'Dex', descricao:'', dinamico:false, baseId:[], operador:[], multiadd:[]},
    {   id:2,   nome:'Força',       sigla:'For', descricao:'', dinamico:false, baseId:[], operador:[], multiadd:[]},
    {   id:3,   nome:'Flexibilidade', sigla:'Fle', descricao:'', dinamico:false, baseId:[], operador:[], multiadd:[]},
    {   id:4,   nome:'Saúde',       sigla:'Sau', descricao:'', dinamico:false, baseId:[], operador:[], multiadd:[]},

    {   id:11,  nome:'Ataque',      sigla:'Atq', descricao:'', dinamico:true, baseId:[2],  operador:[1],    multiadd:[false]},
    {   id:12,  nome:'Defesa',      sigla:'Def', descricao:'', dinamico:true, baseId:[2,4], operador:[1.5,2], multiadd:[false,false]},
    {   id:13,  nome:'Esquiva',     sigla:'Esq', descricao:'', dinamico:true, baseId:[1,3], operador:[2,1.5], multiadd:[false,false]},
    {   id:14,  nome:'Vida',        sigla:'HP',  descricao:'', dinamico:true, baseId:[2,4], operador:[2,4], multiadd:[false,false]},

    {   id:21,   nome:'Dano',        sigla:'Dn', descricao:'', dinamico:true, baseId:[11], operador:[1], multiadd:[false]},
    {   id:22,   nome:'Resistência', sigla:'Rs', descricao:'', dinamico:true, baseId:[12], operador:[1], multiadd:[false]}
];
    return {atributos};
  }
}