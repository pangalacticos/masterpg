import { NgModule }                     from '@angular/core';
import { BrowserModule }                from '@angular/platform-browser';
import { HttpModule, JsonpModule }      from '@angular/http';
import { FormsModule }                  from '@angular/forms';



import { AppComponent }                 from './app.component';
import { Ng2BootstrapModule }           from 'ng2-bootstrap/ng2-bootstrap';
import { NAV_DROPDOWN_DIRECTIVES }      from './shared/nav-dropdown.directive';

import { ChartsModule }                 from 'ng2-charts/ng2-charts';
import { SIDEBAR_TOGGLE_DIRECTIVES }    from './shared/sidebar.directive';
import { AsideToggleDirective }         from './shared/aside.directive';
import { BreadcrumbsComponent }         from './shared/breadcrumb.component';
import { routing }                      from './app.routing';

//Layouts
import { FullLayoutComponent }          from './layouts/full-layout.component';
import { SimpleLayoutComponent }        from './layouts/simple-layout.component';

//Main view
import { DashboardComponent }           from './dashboard/dashboard.component';

//Components
import { AventuraComponent }            from './aventura/aventura.component';
import { PersonagemComponent }          from './personagem/personagem.component';
import { LivroComponent }               from './livro/livro.component';



import { InicialComponent }             from './inicial/inicial.component';
import { LivroDetalheComponent }        from './livro/livro-detalhe.component';
import { AtributoVisualizarComponent }  from './livro/atributo/atributo-visualizar.component';
import { AtributoCadastrarComponent }   from './livro/atributo/atributo-cadastrar.component';
import { AtributoDetailComponent }      from './livro/atributo/atributo-detail.component';
import { AtributoService }              from './livro/atributo/atributo.service';




//Icons


//Widgets
import { WidgetsComponent }             from './widgets/widgets.component';

//Charts
import { ChartsComponent }              from './charts/charts.component';

//Pages
import { p404Component }                from './pages/404.component';
import { p500Component }                from './pages/500.component';
import { LoginComponent }               from './pages/login.component';
import { RegisterComponent }            from './pages/register.component';

@NgModule({
  imports: [
    BrowserModule,
    routing,
    HttpModule,
    FormsModule,
    JsonpModule,
    Ng2BootstrapModule,
    ChartsModule
  ],
  declarations: [
    AppComponent,
    FullLayoutComponent,
    SimpleLayoutComponent,
    DashboardComponent,
    WidgetsComponent,
    ChartsComponent,
    p404Component,
    p500Component,
    LoginComponent,
    RegisterComponent,
    NAV_DROPDOWN_DIRECTIVES,
    BreadcrumbsComponent,
    SIDEBAR_TOGGLE_DIRECTIVES,
    AsideToggleDirective,


    AventuraComponent,
    PersonagemComponent,
    LivroComponent,
    InicialComponent,

    LivroDetalheComponent,
    AtributoVisualizarComponent,
    AtributoCadastrarComponent,
    AtributoDetailComponent
  ],
    providers:[
        AtributoService
    ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
