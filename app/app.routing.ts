import { Routes, RouterModule }         from '@angular/router';

//Layouts
import { FullLayoutComponent }          from './layouts/full-layout.component';
import { SimpleLayoutComponent }        from './layouts/simple-layout.component';

//Main view
import { DashboardComponent }           from './dashboard/dashboard.component';

//Components
import { AventuraComponent }            from './aventura/aventura.component';
import { PersonagemComponent }          from './personagem/personagem.component';
import { LivroComponent }               from './livro/livro.component';
import { LivroDetalheComponent }        from './livro/livro-detalhe.component';
import { AtributoVisualizarComponent }  from './livro/atributo/atributo-visualizar.component';

//Icons

//Widgets
import { WidgetsComponent }             from './widgets/widgets.component';

//Charts
import { ChartsComponent }              from './charts/charts.component';

//Pages
import { p404Component }                from './pages/404.component';
import { p500Component }                from './pages/500.component';
import { LoginComponent }               from './pages/login.component';
import { RegisterComponent }            from './pages/register.component';

const appRoutes: Routes = [
    {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
    },
    {
        path: '',
        component: FullLayoutComponent,
        data: {
            title: 'Home'
        },
        children: [
            {
                path: 'dashboard',
                component: DashboardComponent,
                data: {
                    title: 'Dashboard'
                }
            },
            {
                path: 'aventura',
                component: AventuraComponent,
                data: {
                    title: 'Aventura'
                }
            },
            {
                path: 'personagem',
                component: PersonagemComponent,
                data: {
                    title: 'Personagem'
                }
            },
            {
                path: 'livro',
                component: LivroComponent,
                data: {
                    title: 'Livro'
                },
                children: [
                    {
                        path: '',
                        /*component: LivroDetalheComponent,
                        data: {
                            title: 'Detalhe'
                        }*/
                    },
                    {
                        path: 'detalhe',
                        component: LivroDetalheComponent,
                        data: {
                            title: 'Detalhe'
                        }
                    }
                ]
            },
            {
                path: 'livro/detalhes',
                component: LivroDetalheComponent,
                data: {
                    title: 'Livro   /   Detalhe'
                }
            },
            {
                path: 'livro/detalhes/atributos',
                component: AtributoVisualizarComponent,
                data: {
                    title: 'Livro / Detalhe / Atributos'
                }
            },
            {
                path: 'components',
                redirectTo: 'components/buttons',
                pathMatch: 'full',
            },

            {
                path: 'widgets',
                component: WidgetsComponent,
                data: {
                    title: 'Widgets'
                }
            },
            {
                path: 'charts',
                component: ChartsComponent,
                data: {
                    title: 'Charts'
                }
            }
        ]
    },
    {
        path: 'pages',
        component: SimpleLayoutComponent,
        data: {
            title: 'Pages'
        },
        children: [
            {
                path: '404',
                component: p404Component,
                data: {
                    title: 'Page 404'
                }
            },
            {
                path: '500',
                component: p500Component,
                data: {
                    title: 'Page 500'
                }
            },
            {
                path: 'login',
                component: LoginComponent,
                data: {
                    title: 'Login Page'
                }
            },
            {
                path: 'register',
                component: RegisterComponent,
                data: {
                    title: 'Register Page'
                }
            }
        ]
    }
];

export const routing = RouterModule.forRoot(appRoutes);
